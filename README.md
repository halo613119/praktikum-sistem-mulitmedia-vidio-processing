# Praktikum Sistem Multimedia - Vidio Processing UAS

# No 1
###### Deskripsi:
###### Kodingan di atas adalah contoh implementasi program untuk menghitung push-up menggunakan deteksi pose tubuh (body pose detection) menggunakan MediaPipe dan OpenCV. Program ini menggunakan webcam sebagai sumber video input dan menggambar landmark pose tubuh pada frame video yang ditangkap. Dalam program ini, push-up dihitung berdasarkan pergerakan bahu dan pinggul. Ketika bahu dan pinggul bergerak dalam pola yang menunjukkan satu kali repetisi push-up, maka push-up count akan bertambah. Hasil perhitungan push-up ditampilkan di sudut kiri atas jendela video.
#### Use case:
###### 1. Penghitungan push-up otomatis: Program ini dapat digunakan oleh seseorang yang sedang melakukan latihan push-up untuk menghitung jumlah push-up yang dilakukan tanpa perlu menghitung secara manual. Dengan menggunakan deteksi pose tubuh, program dapat secara otomatis mengenali gerakan push-up dan menghitung jumlah repetisi dengan akurat.
###### 2. Pelatihan olahraga: Program ini juga dapat digunakan oleh pelatih atau instruktur olahraga untuk memantau dan menghitung push-up yang dilakukan oleh atlet atau peserta latihan. Dengan adanya sistem otomatis ini, pelatih dapat fokus pada memberikan arahan dan koreksi teknik, sementara program menghitung jumlah repetisi push-up.
###### 3. Kebugaran pribadi: Bagi individu yang tertarik untuk memantau dan meningkatkan kebugaran mereka sendiri, program ini dapat menjadi alat yang berguna. Mereka dapat menggunakan program ini untuk melacak dan mengukur kemajuan mereka dalam melakukan push-up seiring waktu. Data push-up count dapat dicatat dan dianalisis untuk mengukur perkembangan kebugaran dan menetapkan target yang lebih ambisius.
###### 4. Pengembangan teknologi deteksi pose: Kodingan ini juga dapat digunakan sebagai contoh untuk pengembangan lebih lanjut dalam teknologi deteksi pose dan aplikasi penghitungan berbasis gerakan. Dengan memanfaatkan MediaPipe dan model deteksi pose yang lebih canggih, program ini dapat diadaptasi untuk mendeteksi dan menghitung gerakan tubuh lainnya, seperti squat, plank, atau gerakan lainnya dalam berbagai jenis latihan atau aktivitas fisik.


# No 2
###### Algoritma yang digunakan: 
```mermaid
graph LR
A[Inisialisasi] --> B[Menginisialisasi Webcam]
B --> C[Memulai MediaPipe Pose dengan konfigurasi deteksi dan tracking]
C --> D[Mulai Perulangan]
D --> E[Membaca Frame dari Webcam]
E --> F[Mengubah Warna Gambar ke RGB]
F --> G[Mendeteksi Pose Tubuh Menggunakan MediaPipe]
G --> H[Mengubah Warna Gambar ke BGR]
H --> I[Menampilkan Landmark Pose jika ada]
I --> J[Menghitung Push-Up berdasarkan pergerakan bahu dan pinggul]
J --> K[Menampilkan Push-Up Count]
K --> L[Menampilkan Gambar dengan Push-Up Count]
L --> M[Memeriksa tombol 'q' ditekan]
M --> N[Menghentikan Webcam dan Menutup Jendela]
```
###### Penjelasan Algoritma di atas :
###### 1. Program diinisialisasi dengan mengimpor library yang diperlukan dan mengatur variabel push-up count dan is_counting.
###### 2. Webcam diinisialisasi untuk mengakses video input.
###### 3. MediaPipe Pose dimulai dengan konfigurasi deteksi dan tracking pose tubuh.
###### 4. Perulangan dimulai untuk membaca setiap frame dari webcam.
###### 5. Setiap frame diubah menjadi warna RGB untuk pengolahan menggunakan MediaPipe.
###### 6. MediaPipe digunakan untuk mendeteksi pose tubuh dalam frame.
###### 7. Warna frame dikembalikan ke BGR untuk ditampilkan dengan OpenCV.
###### 8. Jika pose tubuh terdeteksi, landmark pose tubuh ditampilkan pada frame menggunakan OpenCV.
###### 9. Push-up dihitung berdasarkan pergerakan bahu dan pinggul dalam pose tubuh.
###### 10. Jumlah push-up ditampilkan pada frame menggunakan OpenCV.
###### 11. Frame video dengan push-up count ditampilkan pada jendela.
###### 12. Program memeriksa apakah tombol 'q' ditekan untuk menghentikan perulangan.
###### 13. Webcam dihentikan dan jendela ditutup.

# No 3
###### Untuk Aplikasi yang sudah kami buat menggunakan CLI berikan adalah sedikit pengimplementasiannya: 
![demo-image](https://gitlab.com/halo613119/praktikum-sistem-mulitmedia-vidio-processing/-/raw/main/PushUp.gif)

# No 4 
###### Berikut adalah link YouTube untuk penjelasan aplikasi:
https://youtu.be/8hyG5RLZIRk

